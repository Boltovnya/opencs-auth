# OpenCS Auth

OpenCS Auth is a Discord account verification tool for Discord servers. It is designed to be used in tandem with web-hosted server rules to ensure that the rules have been read, understood, and agreed to.

