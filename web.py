import os

from os.path import join, dirname

from flask import Flask, redirect, url_for, render_template
from flask_discord import DiscordOAuth2Session, requires_authorization, Unauthorized
from dotenv import load_dotenv

import requests
import json

app = Flask(__name__)

dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)

BOT_TOKEN = os.environ.get("BOT_TOKEN")
GUILD_ID = os.environ.get("GUILD_ID")
ROLE_ID = os.environ.get("ROLE_ID")
BOT_CHANNEL = os.environ.get("BOT_CHANNEL")

app.secret_key = os.environ.get("FLASK_APP_SECRET")
app.config["DISCORD_CLIENT_ID"] = os.environ.get("DISCORD_CLIENT_ID")
app.config["DISCORD_CLIENT_SECRET"] = os.environ.get("DISCORD_CLIENT_SECRET")
app.config["DISCORD_REDIRECT_URI"] = os.environ.get("DISCORD_REDIRECT_URI")

discord = DiscordOAuth2Session(app)

api_root = "https://discord.com/api"

bot_headers = {
        "Authorization": f"Bot {BOT_TOKEN}",
        "User-Agent": "Heimdall (v0.1)",
        "Content-Type": "application/json",
}

def heimdall_grant(user):
    url = f"{api_root}/api/guilds/{GUILD_ID}/members/{user.id}/roles/{ROLE_ID}"
    r = requests.put(url, headers = bot_headers)
    if r.ok:
        heimdall_say(user)

def heimdall_say(user):
    url = f"{api_root}/channels/{BOT_CHANNEL}/messages"
    r = requests.post(url, headers = bot_headers, json={
        "content": f"Heimdall has judged {user.name}#{user.discriminator} worthy!"
    })
 
@app.route("/")
def home():
    return render_template('index.html')

@app.route("/login/")
def login():
    return discord.create_session(["identify"])

@app.route("/callback/")
def callback():
    discord.callback()
    user = discord.fetch_user()
    heimdall_grant(user)
    return redirect(url_for(".me"))

@app.errorhandler(Unauthorized)
def redirect_unauthorized(e):
    return redirect(url_for("./"))

@app.route("/me/")
@requires_authorization
def me():
    user = discord.fetch_user()
    return render_template('me.html', user=user)

if __name__ == "__main__":
    app.run()
